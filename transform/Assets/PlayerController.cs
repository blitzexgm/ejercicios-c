﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public WaypointController waypointControl;   // Reference to WaypointController Script
    public float speed = 5;
    private GameObject cam;

    private void Start()
    {
        cam = GameObject.Find("Main Camera");  //Get GameObject reference by Name
    }
    void Update ()
    {
        float stop = speed * Time.deltaTime;  //Unity unitss per seconds
        Vector3 currentWaypointPos = waypointControl.getCurrentWaypointIntPos();

        // Move player´s transform to currentWaypoint position
        transform.position = Vector3.MoveTowards(transform.position, currentWaypointPos, stop);

        // Move the camera and the player together
        cam.transform.position = transform.position - Vector3.forward * 10f;
    }
}
