﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp8
{
    class Program
    {
        static int calculadora(int[] newarray, char operacion)
        {
            int total = 0;
            switch (operacion)
            {
                case '+':
                    total = 0;
                    for (int i = 0; i < newarray.Length; i++)
                    {
                        total += newarray[i];
                    }
                    break;
                case '-':
                    total = newarray[0];
                    for (int i = 1; i < newarray.Length; i++)
                    {
                        total -= newarray[i];
                    }
                    break;
                case '*':
                    total = 1;
                    for (int i = 0; i < newarray.Length; i++)
                    {
                        total *= newarray[i];
                    }
                    break;
            }
            return total;
        }
        static void Main(string[] args)
        {
            int[] newarray = {1,2,3,4,5,6};
            Console.WriteLine(calculadora(newarray, '+'));
            Console.WriteLine(calculadora(newarray, '-'));
            Console.WriteLine(calculadora(newarray, '*'));
            Console.ReadKey();
        }
    }
}
