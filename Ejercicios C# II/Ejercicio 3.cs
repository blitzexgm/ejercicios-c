﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp14
{
    class Program
    {
        static void drawRectangle(int columns, int rows)
        {
            for (int i = 0; i < (columns - 1); i++)
            {
                for (int j = 0; j < rows; j++)
                {
                    Console.Write('*');
                }
                Console.WriteLine('*');
            }
        }
        static void Main(string[] args)
        {
            drawRectangle(4, 3);
            Console.ReadKey();
        }
    }
}