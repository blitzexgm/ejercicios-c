﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp19
{
    class Program
    {
        static int columna;
        static int fila;
        static void Main(string[] args)
        {
            Console.WriteLine("Mete texto: ");
            string text = Console.ReadLine();
            Console.WriteLine("Dime donde esta x: ");
            int x = int.Parse(Console.ReadLine());
            Console.WriteLine("Dime donde esta y: ");
            int y = int.Parse(Console.ReadLine());

            try
            {
                Console.SetCursorPosition(columna + x, fila + y);
                Console.Write(text);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
            Console.ReadKey();
        }
    }
}
