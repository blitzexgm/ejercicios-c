﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public static class Encriptator
    {

        public static void Encriptar(ref string texto)
        {

            var kk = texto.ToCharArray();
            for (int i = 0; i < kk.Length; i++)
            {
                kk[i] += (char)1;
            }
            string kk2 = new string(kk);
            texto = kk2;
        }

        public static void Desencriptar(ref string texto)
        {
            var kk = texto.ToCharArray();
            for (int i = 0; i < kk.Length; i++)
            {
                kk[i] -= (char)1;
            }
            string kk2 = new string(kk);
            texto = kk2;
        }
    }

    class Program
    {
        static void Main(string[] args)

        {
            string texto = Console.ReadLine();
            Encriptator.Encriptar(ref texto);
            Console.WriteLine(texto);
            Encriptator.Desencriptar(ref texto);
            Console.WriteLine(texto);
            Console.ReadKey();
        }
    }
}