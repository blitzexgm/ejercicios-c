﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp20
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mete un numero: ");
            int numero = int.Parse(Console.ReadLine());
            Console.WriteLine("Mete la potencia: ");
            int potencia = int.Parse(Console.ReadLine());
            int elevar = 1;
            int[] array = new int[potencia];
            for (int i = 0; i < potencia; i++)
            {
                elevar *= numero;
            }
            Console.WriteLine(elevar);
            Console.ReadKey();
        }
    }
}
