﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    class Program
    {
        static void Main(string[] args)
        {
            int solucion = 1;
            Console.WriteLine("Dime numero: ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = n - i;
            }
            for (int j = 0; j < n-1; j++)
            {
                solucion *= array[j];
            }
            Console.WriteLine(solucion);
            Console.ReadKey();
        }
    }
}
