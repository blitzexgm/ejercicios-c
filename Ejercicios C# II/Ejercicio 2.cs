﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp12
{
    class Program
    {
        public static void newarray(int[] array,ref int min, ref int max)
        {
            min = array[0];
            max = array[0];
            for (int i=0; i< array.Length; i++)
            {
                if (array[i] < min)
                    min = array[i];
                if (array[i] > max)
                    max = array[i];
            }
        }
        static void Main(string[] args)
        {
            int Max = 0;
            int Min = 0;
            int[] lista = { 1, 2, 3, 4 };
            newarray(lista, ref Min, ref Max);
            Console.WriteLine("El minimo: {0}", Min);
            Console.WriteLine("El maximo: {0}", Max);
            Console.ReadKey();
        }
    }
}
