﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp15
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mete texto: ");
            string texto = Console.ReadLine();
            var caracteres = texto.ToCharArray();
            int[] array = new int[caracteres.Length];
            for (int i = 0; i< caracteres.Length; i++)
            {
                if (caracteres[i] == ' ')
                {
                    caracteres[i] = '_';
                }
                if (caracteres[i] == 'a' || caracteres[i] == 'e' || caracteres[i] == 'i'|| caracteres[i] == 'o'|| caracteres[i] == 'u')
                {
                    caracteres[i] -= (char)32;
                }
                array[i] = caracteres[i];
                Console.Write(array[i]);
            }
            Console.WriteLine();
            for (int j = 0; j < caracteres.Length; j++)
            {
                Console.Write(caracteres[j]);
            }
            Console.ReadKey();
        }
    }
}
