﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp21
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Introduce un texto: ");
            string word = Console.ReadLine();
            var array = word.ToCharArray();
            int tam = array.Length;
            var array2 = new char[tam];


            for (int i = 0; i < (tam / 2 + 1); i++)
            {
                array2[i] = array[(tam - 1) - i];
                array2[i + (tam / 2)] = array[(tam - 1) - (i + (tam / 2))];

            }
            if (array == array2)
            {
                Console.WriteLine("Esta es un palindromo");
            }
            else
            {
                Console.WriteLine("Esta no es un palindromo");
            }
            Console.WriteLine(array);
            Console.WriteLine(array2);
            Console.ReadKey();


        }
    }
}