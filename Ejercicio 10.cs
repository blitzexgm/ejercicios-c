﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] reglafibonacci = new int[27];
            reglafibonacci[0] = 0;
            reglafibonacci[1] = 1;
            for(int i=2; i < 27; i++)
            {
                reglafibonacci[i] = reglafibonacci[i - 1] + reglafibonacci[i - 2];
                Console.WriteLine(reglafibonacci[i]);
            }
            Console.ReadLine();
        }
    }
}
