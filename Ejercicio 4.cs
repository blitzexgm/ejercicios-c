﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Introduce un año");
            string cadena = Console.ReadLine();
            int ano = int.Parse(cadena);
            if (ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0)
            {
                Console.WriteLine("Es bisiesto");
            }
            else Console.WriteLine("No es bisiesto");
            Console.ReadKey();
        }
    }
}
