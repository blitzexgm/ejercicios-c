﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp9
{
    class Program
    {
        static bool numero(int[,] arraybi, int buscanumero)
        {
            for (int i = 0; i< arraybi.GetLength(0); i++)
            {
                for(int h = 0; h<arraybi.GetLength(1); h++)
                {
                    if (arraybi[i, h] == buscanumero)
                        return true;
                }
            }
            return false;
        }
        static void Main(string[] args)
        {
            int[,] newarray = new int[2, 2] { { 1, 2 }, { 3, 4 } };
            if (numero(newarray, 3))
                Console.WriteLine("El numero esta dentro del array");
            else
                Console.WriteLine("El numero no esta dentro del array");
            Console.ReadKey();
        }
    }
}
