﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero;

            for (numero = 1; numero <= 100; numero++)
            {
                if (numero % 3 != 0 && numero % 5 != 0)
                {
                    Console.WriteLine(numero);
                    
                }
            }
            Console.ReadKey();
        }
    }
}
