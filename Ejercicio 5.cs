﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] dado = new int[8];
            Random something = new Random();
            for (int tirada = 0; tirada < 50; tirada++)
            {
                int variable = something.Next(0, 6);
                dado[variable] += 1;

            }
            Console.WriteLine("el numero 1 se ha repetido {0} veces", dado[0]);
            Console.WriteLine("el numero 2 se ha repetido {0} veces", dado[1]);
            Console.WriteLine("el numero 3 se ha repetido {0} veces", dado[2]);
            Console.WriteLine("el numero 4 se ha repetido {0} veces", dado[3]);
            Console.WriteLine("el numero 5 se ha repetido {0} veces", dado[4]);
            Console.WriteLine("el numero 6 se ha repetido {0} veces", dado[5]);
            Console.ReadKey();
        }
        
    }
}
