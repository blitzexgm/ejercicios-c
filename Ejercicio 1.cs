﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio1
{
    class Program
    {
        static void Main(string[] args)
        {
            int sumatoriopar = 0;

            for (int numero = 2; numero <= 499; numero++)
            {
                if (numero / 2 == 0)
                    sumatoriopar += numero;
            }

            Console.WriteLine(sumatoriopar);

            int sumatorioimpar = 0;
            for (int numero = 2; numero <= 499; numero++)
            {
                if (numero / 2 != 0)
                    sumatorioimpar += numero;
            }
            Console.WriteLine(sumatorioimpar);
            Console.ReadKey();
        }
    }
}
